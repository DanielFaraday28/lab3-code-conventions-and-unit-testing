import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.controllers.threadController;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class threadControllerTests {

    private List<Post> testPosts;
    private Thread testThread, secondThread;
    private User testUser;

    private final String existingNickname = "Faraday";
    private final String existingSlug = "testSlug";

    @BeforeEach
    @DisplayName("Thread creation test")
    void createForumTest() {
        testThread = new Thread(
                existingNickname,
                new Timestamp(12321421421L),
                "testForum",
                "testMessage",
                existingSlug,
                "testTitle",
                10
        );
        testThread.setId(0);
        secondThread = new Thread(
                existingNickname,
                new Timestamp(1232142L),
                "testForum",
                "secondMessage",
                existingSlug,
                "secondTitle",
                11
        );
        secondThread.setId(1);
        testUser = new User(
                existingNickname,
                "test@mail.ru",
                "Peter Zakharkin",
                "Random Info"
        );
        Post testPost = new Post(
                existingNickname,
                new Timestamp(12321421421L),
                "testForum",
                "testMessage",
                0,
                0,
                false
        );
        testPosts = new LinkedList<>();
        testPosts.add(testPost);
    }

    @Test
    @DisplayName("Check ID or Slug")
    void correctlyCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(0)).thenReturn(testThread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(existingSlug)).thenReturn(testThread);

            threadController controller = new threadController();

            assertEquals(controller.CheckIdOrSlug(existingSlug), testThread);
            assertEquals(controller.CheckIdOrSlug("0"), testThread);
            assertNull(controller.CheckIdOrSlug("noSlug"), "Checking non-existing slug returns null");
            assertNull(controller.CheckIdOrSlug("1"), "Checking non-existing id returns null");
        }
    }

    @Test
    @DisplayName("Create Post")
    void correctlyCreatePost() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(existingSlug)).thenReturn(testThread);
                userMock.when(() -> UserDAO.Info(existingNickname)).thenReturn(testUser);

                threadController controller = new threadController();

                // Test for correct return behaviour
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(testPosts), controller.createPost(existingSlug, testPosts));
            }
        }
    }

    @Test
    @DisplayName("Get Posts")
    void correctlyGetPosts() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(existingSlug)).thenReturn(testThread);
            threadMock.when(() -> ThreadDAO.flatSort(0, 1, 0, false)).thenReturn(testPosts);

            threadController controller = new threadController();

            // Test for correct return behaviour
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(Collections.emptyList()), controller.Posts(existingSlug, 1, 0, null, false));
        }
    }

    @Test
    @DisplayName("Update Post Details")
    void correctlyUpdatePostDetails() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(existingSlug)).thenReturn(secondThread);
            threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(secondThread);

            threadController controller = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(secondThread), controller.change(existingSlug, secondThread));
        }
    }

    @Test
    @DisplayName("Get Post Details")
    void correctlyGetPostDetails() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(existingSlug)).thenReturn(testThread);

            threadController controller = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(testThread), controller.info(existingSlug));
        }
    }

    @Test
    @DisplayName("Create Vote")
    void correctlyCreateVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(existingSlug)).thenReturn(testThread);
                userMock.when(() -> UserDAO.Info(existingNickname)).thenReturn(testUser);

                threadController controller = new threadController();
                Vote vote = new Vote("Faraday", 1);

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(testThread), controller.createVote(existingSlug, vote));
            }
        }
    }

}